Experimental project

This is a sandbox project, which contains experimental code for developer use only.

A ticket message manager that relies on workflow and requires to have the ticket bundle and the maintainer role.

When someone comments a ticket it will inform the creator and the members in maintainer role.
When someone creates a ticket it will notify the members in maintainer role.
When someone updates the workflow of a ticket it will notify the creator of the ticket.

During render of ticket node to mail Will use teaser view mode, it is because we need a view mode that is not rendering the workflow form, if it is there we enter in an infinite loop of sending node_update, if form is needed in teaser we must create/use another view mode.

