<?php

/**
* @file providing the service that manages tickets message.
*
*/

namespace Drupal\tickets_message;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Logger\LoggerChannel;
use Psr\Log\LoggerInterface;
use Drupal\Core\Entity\EntityInterface;

class TicketsMessageManager {

  /**
   * QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * The mail manager under test.
   *
   * @var \Drupal\Core\Mail\MailManager
   */
  protected $mailManager;

  /**
   * The logger.
   * @var \Psr\Log\LoggerInterface;
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The query factory.
   * @param \Drupal\Core\Mail\MailManager $mail_manager
   *   The mail manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger
   */
  public function __construct(QueryFactory $entity_query, MailManager $mail_manager, LoggerInterface $logger) {
    $this->entityQuery = $entity_query;
    $this->mailManager = $mail_manager;
    $this->logger = $logger;
  }

  /**
   * Gets the users of maintainer role.
   *
   * @return array An array of user objects indexed by their IDs;
   *
   */
  public function maintainersList() {
    $users = [];
    $query = $this->entityQuery->get('user');
    $query->condition('roles', 'maintainer');
    $entity_ids = $query->execute();
    if (!empty($entity_ids)){
      $users = entity_load_multiple('user',$entity_ids);
    }
    return $users;
  }

  /**
   * If $field_name is not known, yet, determine it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to inspect.
   * @param string $field_name
   *   The field name if is known.
   *
   * @returns string|null
   *   The field name if any.
   */
  public function getWorkflowFieldName(EntityInterface $entity, $field_name = '') {
    if (!$entity) {
      return NULL;
    }
    // If $field_name is not known, yet, determine it.
    $field_name = ($field_name) ? $field_name : workflow_get_field_name($entity, $field_name);
    // If $field_name is found, get more details.
    if (!$field_name || !isset($entity->$field_name)) {
      // Return the initial value.
      return NULL;
    }
    return $field_name;
  }
  
  /**
   * Calls drupal_mail() to notify users.
   *
   * @param string $new_state
   *   The state of the ticket.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that notification is referencing.
   * @param string $field_name
   *   The name of the field that provides the workflow @deprecated.
   * @param null $transition
   *   The transition of state of the ticket .
   * @param null $user
   *   The user that is about to be notified @deprecated.
   * @param string $op
   *   The opeation that triggers this notification.
   *
   * @throws EntityMalformedException
   *
   * @see tickets_message_mail()
   */
  function notify($new_state, $entity,$field_name = '', $transition = NULL, $user = NULL, $op = NULL ) {
    $entity_type = $entity->getEntityTypeId();
    $module = 'tickets_message';

    $params['node'] = ($op == 'comment')? $entity->getCommentedEntity() : $entity;
    $params['node_title'] = $params['node']->label();

    if ($op == 'update'){
      $key = 'ticket_update';
      $to = [ $entity->getOwner()->id() => $entity->getOwner() ];
      $params['state'] = $new_state;
      $params['transition'] = $transition;
    }
    if ($op == 'create'){
      $key = 'ticket_create';
      $to = $this->maintainersList();
      $params['from'] = $entity->getOwner()->getEmail();
    }
    if ($op == 'comment'){
      $key = 'ticket_comment';
      $to = $this->maintainersList();
      $to['owner'] = $entity->getOwner();
      $params['comment'] = $entity;
    }
    $send = true;
    foreach ($to as $uid => $user ){
      if ($op == 'comment'){
        $params['maintainer'] = $uid != 'owner';
      }
      $recipient = $user->getEmail();
      $langcode = $user->getPreferredLangcode();
      $result = $this->mailManager->mail($module, $key, $recipient, $langcode, $params, NULL, $send);
      if (!$result['result']) {
        $message = t('There was a problem sending your email notification to @email affecting node @id.', array('@email' => $recipient, '@id' => $entity->id()));
        //drupal_set_message($message, 'error');
        $this->logger->error($message);
        return;
      }
      $message = t('An email notification has been sent to @email affecting node @id.', array('@email' => $recipient, '@id' => $entity->id()));
      //drupal_set_message($message);
      $this->logger->notice($message);
    }
  }
}

