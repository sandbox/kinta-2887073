<?php

/**
 * @file
 * Preprocessors and theme functions of Tickets message module.
 */

use Drupal\Core\Render\Element;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Prepares variables for ticket update template.
 *
 * Default template: tickets-update.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An array of elements to display.
 */
function template_preprocess_tickets_update(&$variables) {
  // Get stowaway variables and unset after
  $variables['state'] = $variables['element']['state'];
  $variables['transition'] = $variables['element']['transition'];

  unset($variables['element']['state']);
  unset($variables['element']['transition']);


  $item = $variables['element']['#node'];

  // Helpful $content variable for templates.

  foreach (Element::children($variables['element']) as $key) {
    $variables['content'][$key] = $variables['element'][$key];
  }

  $owner = $item->getOwner();
  $variables['owner_id'] = $owner->id();
  $variables['owner_name'] = $owner->getDisplayName();
  //$variables['url'] = UrlHelper::stripDangerousProtocols($item->toLink('#' . $item->id(), 'canonical', ['absolute' => TRUE ] ));
  $variables['url'] = $item->toLink('#' . $item->id(),             'canonical', ['absolute' => TRUE ] );
  $panel_url =  Url::fromUserInput('/user/' . $owner->id() . '/tickets',[ 'absolute' => TRUE ]);
  $variables['panel_url'] = Link::fromTextAndUrl(t('ticket panel'), $panel_url )->toString();

}

/**
 * Prepares variables for ticket create template.
 *
 * Default template: tickets-create.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An array of elements to display.
 */
function template_preprocess_tickets_create(&$variables) {
  $item = $variables['element']['#node'];

  // Helpful $content variable for templates.

  foreach (Element::children($variables['element']) as $key) {
    $variables['content'][$key] = $variables['element'][$key];
  }

  $owner = $item->getOwner();
  $variables['owner_id'] = $owner->id();
  $variables['owner_name'] = $owner->getDisplayName();
  //$variables['url'] = UrlHelper::stripDangerousProtocols($item->toLink('#' . $item->id(), 'canonical', ['absolute' => TRUE ] ));
  $variables['url'] = $item->toLink('#' . $item->id(),             'canonical', ['absolute' => TRUE ] );
  $admin_panel_url = Url::fromUserInput('/admin/content/tickets',[ 'absolute' => TRUE ]);
  $variables['admin_panel_url'] = Link::fromTextAndUrl(t('admin tickets panel'), $admin_panel_url)->toString();
}

/**
 * Prepares variables for ticket comment template.
 *
 * Default template: tickets-comment.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An array of elements to display.
 */
function template_preprocess_tickets_comment(&$variables) {
  $item = $variables['element']['#node'];
  $variables['maintainer'] = $variables['element']['maintainer']['#plain_text'];

  unset($variables['element']['maintainer']);


  // Helpful $content variable for templates.

  foreach (Element::children($variables['element']) as $key) {
    $variables['content'][$key] = $variables['element'][$key];
  }

  $owner = $item->getOwner();
  $variables['owner_id'] = $owner->id();
  $variables['owner_name'] = $owner->getDisplayName();
  //$variables['url'] = UrlHelper::stripDangerousProtocols($item->toLink('#' . $item->id(), 'canonical', ['absolute' => TRUE ] ));
  $variables['url'] = $item->toLink('#' . $item->id(),             'canonical', ['absolute' => TRUE ] );
  $admin_panel_url = Url::fromUserInput('/admin/content/tickets',[ 'absolute' => TRUE ]);
  $variables['admin_panel_url'] = Link::fromTextAndUrl(t('admin tickets panel'), $admin_panel_url)->toString();
  $panel_url =  Url::fromUserInput('/user/' . $owner->id() . '/tickets',[ 'absolute' => TRUE ]);
  $variables['panel_url'] = Link::fromTextAndUrl(t('ticket panel'), $panel_url )->toString();
}

